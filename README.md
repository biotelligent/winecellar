# Wine Cellar Sample Application in Titanium Alloy #

This shows how the new Alloy and Backbone MVC framework can be applied to the wine cellar backbone demonstration.

"WineCellar" is a sample application which allows you to browse through a list of wines, add, update, and delete wines.

The in-memory datastore allows you to try the app without setting up a back-end.
To use the app with the persistent RESTful back-end, set up the database as documented below and clear your settings or reinstall the app on your ios device.

## Original Tutorial ##

This application is documented [here](http://coenraets.org/blog).
The original tutorials are [here](https://github.com/ccoenraets/backbone-cellar/tree/master/tutorial):
The web version of the application is also hosted online. You can test it [here](http://coenraets.org/backbone-cellar/bootstrap).

## Database Set Up (Optional): ##

The database and API backend can be found in the webservice/bootstrap/api folder.

1. Create a MySQL database name "cellar".
2. Execute cellar.sql to create and populate the "wine" table:

	mysql cellar -uroot < cellar.sql

## REST Service Set Up (Optional): ##

1. Edit webservice/api/index.php and set the correct dbuser password for your mySQL install
2. Set up your webserver to be able to access the webservice/bootstrap/api folder
  e.g. add a directory configuration to httpd.conf
  or   set followsymlinks on
  		 sudo ln -s ~/Develop/TitaniumWorkspace/winecellar/webservice/bootstrap/api /Library/Webserver/Documents/cellar
3. Verify these configurations locally from your browser - you should see all SQL records
   http://127.0.0.1/cellar/api/index.php/wines
   http://127.0.0.1/cellar/api/wines/1 [ie. must also work without specifying index.php]
4. Verify you can connect externally from the iOS simulator and your device by browsing
   (iOS simulator and device require the hostname or IP to connect)
   http://yourservername/cellar/api/wines  
5. Run the app, and specify restadapter as the adapter type on the settings page
6. Specify the URL

7. To get back to the settings page, either reset the iOS simulator settings, or uninstall the app (device)
