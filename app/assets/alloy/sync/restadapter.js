// REST adapter for Titanium Alloy
//
// From this gist: https://gist.github.com/2353736#file_backbone.sync.js

function Sync(model, method, options) {
  Ti.API.info("Using Alloy/Backbone restadapter");

  // Map backbone methods onto REST verbs
  var methodMap = {
    'create' : 'POST',
    'read' 	 : 'GET',
    'update' : 'PUT',
    'delete' : 'DELETE'
  }; 

  // Helper functions
  var getValue = function(object, prop) {
    if (!(object && object[prop])) return null;
    return _.isFunction(object[prop]) ? object[prop]() : object[prop];
  };

  // Throw an error when a URL is needed, and none is supplied.
  var urlError = function() {
    throw new Error('A "url" property or function must be specified');
  };
  
  var getUrl = function(model) {
    if (!(model && model.url)) return null;
    var base = getValue(model, 'urlRoot') || getValue(model, 'url') || getValue(model.collection, 'url') || urlError();
    if (undefined === model.collection) return base;
    if (model.isNew()) return base;
    return base + (base.charAt(base.length - 1) == '/' ? '' : '/') + encodeURIComponent(model.id);
  };

  var xhr = Ti.Network.createHTTPClient({
    timeout : 35000
  });

  var type = methodMap[method], params = _.extend({}, options);

  //==== Start standard Backbone.sync code ====
  // Ensure that we have a URL
  if (!params.url)
    params.url = getUrl(model) || urlError();

  // Ensure that we have the appropriate request data - if using rails on server restore toParams() call (refer Gist)
  if (!options.data && model && (method == 'create' || method == 'update')) {
    params.contentType = 'application/json';
    params.data = JSON.stringify(model);
  }

  // For older servers, emulate JSON by encoding the request into an HTML-form.
  if (Backbone.emulateJSON) {
    params.contentType = 'application/x-www-form-urlencoded';
    params.processData = true;
    params.data = params.data ? {
      model : params.data
    } : {};
  }

  // For older servers, emulate HTTP by mimicking the HTTP method with `_method`
  // And an `X-HTTP-Method-Override` header.
  if (Backbone.emulateHTTP) {
    if (type === 'PUT' || type === 'DELETE') {
      if (Backbone.emulateJSON)
        params.data._method = type;
      params.type = 'POST';
      params.beforeSend = function(xhr) {
        xhr.setRequestHeader('X-HTTP-Method-Override', type);
      };
    }
  }

  //==== End standard Backbone.sync code ====

  //Handle success
  xhr.onload = function() {
    var cookies = this.getResponseHeader('Set-Cookie');
    if (cookies != null && Ti.Android)
      Ti.App.Properties.setString('cookies', cookies);

    params.success(JSON.parse(this.responseText));
  };

  //Handle error
  xhr.onerror = params.error;

  //Prepare the request
  xhr.open(type, params.url);

  //Add request headers etc.
  if (params.contentType)
    xhr.setRequestHeader('Content-Type', params.contentType);
  xhr.setRequestHeader('Accept', 'application/json');
  if (params.beforeSend)
    params.beforeSend(xhr);

  if (Ti.Android && Ti.App.Properties.getString('cookies'))
    xhr.setRequestHeader('Cookie', Ti.App.Properties.getString('cookies'));

  //Make the request
  xhr.send(params.data);

  return;
};

var Alloy = require("alloy"), _ = require("alloy/underscore")._;
module.exports.sync = Sync;
