migration.up = function(db) {
	db.createTable({
		"columns": {
			"name":"varchar",
			"year":"varchar",
			"grapes":"varchar",
			"country":"varchar",
			"region":"varchar",
			"description":"text",
			"picture":"varchar"
		},
		"adapter": {
			"type": "sql",
			"collection_name": "wine"
		}
	});
};

migration.down = function(db) {
	db.dropTable("wine");
};
