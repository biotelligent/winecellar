/* 
 * A row comprising the label - multiline-textarea pair for input or viewing the wine description
 */ 
var args = arguments[0] || {};
$.rowView.top = args.top || 0;
$.rowLabel.text = args.text || "";
$.rowView.backgroundColor = args.backgroundColor || $.rowView.backgroundColor;

if (undefined !== args.returnKeyType) {
  if ("done" == args.returnKeyType) {
    $.rowField.returnKeyType = Ti.UI.RETURNKEY_DONE;
  } else if ("default" == args.returnKeyType) {
    $.rowField.returnKeyType = Ti.UI.RETURNKEY_DEFAULT;
  }
}
