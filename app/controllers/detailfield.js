/* A row comprising a label - field pair for input or viewing 
 * 
 * Takes the key=value arguments from the calling xml to set specific attributes
 * Refer http://docs.appcelerator.com/titanium/3.0/#!/guide/Alloy_Controllers Passing Arguments 
 */ 
var args = arguments[0] || {};
$.rowView.top = args.top || 0;
$.rowLabel.text = args.text || "";
$.rowView.backgroundColor = args.backgroundColor || $.rowView.backgroundColor;

// We want to capitalize the WINE NAME field
if (args.capitalize) {
  $.rowField.autocapitalization = Ti.UI.TEXT_AUTOCAPITALIZATION_ALL;
} else {
  $.rowField.autocapitalization = Ti.UI.TEXT_AUTOCAPITALIZATION_WORDS;
}

if (undefined !== args.returnKeyType) {
  if ("done" == args.returnKeyType) {
    $.rowField.returnKeyType = Ti.UI.RETURNKEY_DONE;
  } else if ("default" == args.returnKeyType) {
    $.rowField.returnKeyType = Ti.UI.RETURNKEY_DEFAULT;
  }
}
