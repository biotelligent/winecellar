/*
 * TableView list of wines
 */
var args = arguments[0] || {};
var window = args.window || app.UI.MainWindow;
var needsRefresh = false;

// Perform transformation on each model as it is processed for the UI display.
// We don't want to change the underlying model, instead return a new object with the
// custom fields to use in the display/bindings.
function transformFunction(model) {
  if (undefined !== model) {
    var transform = model.toJSON();

    // rowtitle is the custom field displayed as the row title
    transform.rowtitle = model.get("name") + ' [' + model.get("year") + ']';

    // wineid is a custom attribute which we can then access from rowData when the tableview is clicked
    transform.wineid = model.id;
    return transform;
  }
};

function handleWineAttributeChange(wine) {
  // If the user edited any wine field which affects the sort order, set a flag to re-sort
  app.logInfo("handleWineAttributeChange, set to re-sort list: needsRefresh: " + needsRefresh);
  needsRefresh = true;
}

function handleDetailWindowClose(e) {
  // If the user edited any wine field which affected the sort order, re-sort the list now

  app.logInfo("Callback for closure of detailwindow was triggered: needsRefresh: " + needsRefresh);
  if (needsRefresh) {
    needsRefresh = false;
    app.Wines.sort;
    app.Wines.fetch();
  }
  return;
};

function handleListSelect(e) {
  // The user has selected a row in the list
  var wine = app.Wines.get(e.rowData.wineid);
  if (!app.isNull(wine)) {
    wine.on("change:name", handleWineAttributeChange);
    wine.on("change:year", handleWineAttributeChange);
    app.UI.showDetailWindow(wine, handleDetailWindowClose);
  } else {
    Ti.API.error("Cannot open detail window - list row has no valid wineid: " + JSON.stringify(e));
  }
  return;
};

function handleAddWine(e) {
  var newWine = Alloy.createModel("Wine", {});
  app.UI.showDetailWindow(newWine);
  return;
};

$.searchBtn.on("click", function(e) {
  $.tableview.search = $.search;
  $.tableview.searchHidden = !$.tableview.searchHidden;
});

$.addWineBtn.on("click", handleAddWine);
$.tableview.on("click", handleListSelect);

if (OS_IOS) {
  $.winelistWin.setLeftNavButton($.searchBtn);
  $.winelistWin.setRightNavButton($.addWineBtn);
  $.winelistWin.showNavBar();
}

// Execute an asynchronous fetch to populate the bound listview.
app.Wines.fetch({
  // Not strictly necessary, but prevents the collection needing to do any sort work on the initial fetch
  query : {
    "sql" : "order by name",
    "params" : [],
  },
  success : function(collection, response) {
    // On initial load, populate the wines from memorystore
    if (collection.length == 0) {
      app.logInfo("Performing first data population"); 
      app.Wines.populateData(false);
    }
    return app.logInfo("WineList fetch.success count " + app.Wines.models.length);
  },
  error : function(e) {
    return Ti.API.error(JSON.stringify(e));
  }
});
