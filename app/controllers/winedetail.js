/*
 * Wine item view for browsing, editing, and adding
 * The wine to view is passed in as an argument, as well as whether edit mode is desired
 */
var args = arguments[0] || {};
var wine = args.wine || this.wine;
var editing = args.edit || app.isNull(wine) ? false : wine.isNew();

// Set to true if you want to validate and/or rollback invalid fields at the time of fieldExit
var fieldLevelValidation = true;    // Obviously these only fire if the user enters the field, and changes the value
var rollbackInvalidFields = true;

// Callback for the close event
if (args.onclose) {
  $.winedetailWin.on("close", args.onclose);
};

function handleFieldExit(e) {
  // Update the model values as the user exits the field - this will propogate through to the bound controls in the list etc.
  var fieldName = e.source.fieldName;
  if (e.value != wine.get(fieldName)) {
    app.logInfo("Changing wine." + fieldName + " to " + e.value + " for " + wine.get("name"));

    // Note you can specify silent: true so that validation does not occur on exiting individual fields
    // Validation of fields (see wine.js validate) will also occur when wine.save() is called.
    if (fieldLevelValidation) {
      if (!wine.set(fieldName, e.value)) {
        // Rollback the change if validation fails
        app.logInfo("Failed change from wine." + fieldName + " to " + e.value + " for " + wine.get("name"));
        if (rollbackInvalidFields) {
          if (e.source.value != wine.previous(fieldName)) {
            e.source.value = wine.previous(fieldName);
          }
        } else {
          e.source.backgroundColor = 'red';
        }
      }
    }
  }
  return;
};

function handleDiscardChanges(e) {
  // The user made a new wine, or changes to an existing wine that are incomplete or invalid
  // Cancel the changes and return to the listview
  if (wine.isNew()) {
    wine.destroy();
  } else/*if (wine.hasChanged())*/ {
    // Reload the wine from the database
    wine.fetch();
  }  

  app.UI.navController.close();
  return;  
};

function handleBackClick(e) {
  // Overrides the back button and prevents window closure if details are not valid
  app.logInfo("winedetailWin.backClick - validating wine.");
  
  if (editing) {
    wine.logInfo("DetailWindow back clicked, saving");
    wine.off("error", handleValidationError);
    if (!wine.isValid()) {
      app.UI.showConfirmDialog(
         {title: "Wine details are not valid - cannot save.", options: ["Edit", "Discard changes"], destructive: 1},
          function(e){
            app.logInfo("Returning to edit mode");
            wine.on("error", handleValidationError);
          },
          handleDiscardChanges
        );
      return;
    }
    
    if (wine.isNew()) {
      wine.saveAndAdd(app.Wines);
    } else {
      wine.save();
    }
  }
  app.UI.navController.close();
};

function handleValidationError(wine, error) {
  app.UI.showAlertDialog("Cannot save wine, " + error);
  return;
};

function handleEditClick(e) {
  editing = true;
  render(wine);
  
  return;
};

// Set the field properties, custom data is stored and retrieved in fieldName
function setDetailField(field, fieldName) {
  field.rowLabel.text = (fieldName == "description") ? "Notes:" : app.UI.properCase(fieldName) + ":";
  field.rowField.value = app.isNull(wine) ? "" : wine.get(fieldName);
  field.rowField.fieldName = fieldName;
  field.rowField.editable = editing;
  field.rowField.enabled = editing;
  field.rowField.borderStyle = (editing) ? Titanium.UI.INPUT_BORDERSTYLE_LINE : Titanium.UI.INPUT_BORDERSTYLE_NONE;
  field.rowField.on("blur", handleFieldExit);
};

function render(wine) {
  if (!app.isNull(wine)) {
    this.wine = wine;
    setDetailField($.name, "name");
    setDetailField($.grapes, "grapes");
    setDetailField($.country, "country");
    setDetailField($.region, "region");
    setDetailField($.year, "year");
    setDetailField($.description, "description"); 
    var imagename = wine.get("picture") || "generic.jpg";
    var picture = app.UI.imageFile(imagename);
    if (!app.isNull(picture)) { 
      $.winebottleImage.image = picture;
    } else {
      $.winebottleImage.image = app.UI.imageFile("generic.jpg");      
    }
    if (editing) {
      wine.on("error", handleValidationError);
    } 
  }
  
  if (OS_IOS) {
    if (editing) {
      // Override the default navigationgroup button, to prevent closure if validation fails
      $.winedetailWin.setLeftNavButton($.backBtn);
      $.winedetailWin.setRightNavButton(null);  
    } else {  
      $.winedetailWin.setLeftNavButton(null);
      $.winedetailWin.setRightNavButton($.editBtn);
    }
    $.winedetailWin.showNavBar();
  }
};

$.backBtn.on("click", handleBackClick);
$.editBtn.on("click", handleEditClick);
render(wine);
