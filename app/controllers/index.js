// Environment sanity checks
if (parseFloat(Ti.version) < 3.0) {
  throw "Wrong SDK version (minimum 3.0): " + Ti.version;
}
if (Ti.Platform.osname != "iphone") {
  throw Ti.Platform.osname + " not supported. (Only tested and formatted for iPhone).";
}

// Setup an empty root window, which is responsible for application open/close
$.index.open();
app.UI.MainWindow = $.index;

// If this is a new install, or settings are invalid, show the settings page...
app.logInfo("Reading settings");
app.Settings.fetch({
  success : function(collection, response) {
    if (collection.length == 0) {
      app.logInfo("New settings detected", ["settings"]); 
      return app.UI.showSettingsWindow(app.Settings);
    } else {
      var currentSetting = app.Settings;    
      if ((currentSetting.get("hasEverAuthenticated") == 0) 
      || (currentSetting.get("userName") == "") 
      || (currentSetting.get("adapterType") == "")) {
        app.logInfo("Invalid settings detected", ["settings"]); 
        return app.UI.showSettingsWindow(currentSetting);
      }
    }    
    
    // If we are here, settings are good. Launch the list window.
    app.logInfo("Settings fetch.success", ["settings"]);
    return app.UI.showListWindow($.index);    
  },
  error : function(e) {
    // Really can't do anything - delete and reinstall?
    alert("Problem reading settings.\nPlease restart this application or your device.\n\nIf the problem persists, uninstall and reinstall the application,")
    return Ti.API.error(JSON.stringify(e));
  }
});