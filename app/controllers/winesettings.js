/* 
 * User definable (saved) settings for the application.
 * 
 * Alloy.Models.settings is the global/current record, defined and databound to the rows in winesettings.xml
 */
var currentSettings = app.Settings;

function listProperties() {
  // Dump all the properties stored by Ti. This should include our settings!
  var props = Ti.App.Properties.listProperties();

  app.logInfo("Listing properties", ["settings"]);
  for (var i = 0, ilen = props.length; i < ilen; i++) {
    var value = Ti.App.Properties.getString(props[i]);
    if (value === undefined) {
      value = JSON.stringify(Ti.App.Properties.getObject(props[i]));
    }
    
    if (props[i].substr(0, 6) != "WebKit") {
      app.logInfo(props[i] + ' = ' + value, ["settings"]);
    }
  };
};

function handleSave(e) {
  app.logInfo("Saving settings: ", ["settings"]);
  app.logInfo("urlRoot: " + currentSettings.get("urlRoot"));
  
  // Validate settings
  var isNew = currentSettings.isNew(); 
  var saveOK = false;
  var settingsVerified = false;

  currentSettings.save({
    adapterType: $.adapterType.value,
    userName: $.userName.value,
    urlRoot: $.urlRoot.value,
  },
  {
    success: function(e){ 
      app.logInfo("Settings saved", ["settings"])
      saveOK = true;
    },
    error: function(e){ 
      listProperties();
      return Ti.API.error("Save failed", ["br"]);
    },
  });
  
  if (saveOK) {  
    currentSettings.set("hasEverAuthenticated", 1);
    currentSettings.save();      
    settingsVerified = true; 
  }
  
  listProperties();

  if (settingsVerified) {
    // If this screen was opened as the landing page rather than a menu option, don't use go back 
    if (app.UI.navController.length > 0) {
      // Reload with the new adapter
      app.Wines = null;
      app.initAdapter();
      
      // Return to the parent
      app.UI.navController.close();
    } else {  
      // Boot the list    
      app.UI.showListWindow(app.UI.MainWindow);

      // And close ourself
      $.winesettingsWin.close();
    }
  }
  return;
};

function handleResetData(e) {
  // Clear all local data  -- utility function to assist testing initial/reload
  app.initAdapter();
  if ((app.Wines.length > 0) /*&& (app.Settings.adapterType == "sql")*/) {
    app.Wines.clearData();
  }  
  app.Wines.populateData(true);  
  app.Wines.fetch();
};

if ($.resetBtn){
  $.resetBtn.on("click", handleResetData);    
}

$.saveSettingsBtn.on("click", handleSave);