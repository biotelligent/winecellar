/*
 * The backbone data model for a wine and winecollection
 */
var Alloy = require("alloy");
exports.definition = {
	
	config: {
		"columns": {
			"name":"varchar",
			"year":"varchar",
			"grapes":"varchar",
			"country":"varchar",
			"region":"varchar",
			"description":"text",
			"picture":"varchar"
		},
		"adapter": {
			"type": Alloy.globals.sqlAdapter,
			"collection_name": "wine"
		}
	},		

	extendModel: function(Model) {		
		_.extend(Model.prototype, {
		  
		  urlRoot: Alloy.globals.urlRoot,
			
			// Override the model validation routine, called on model.save()			
      validate: function(attrs) {
        for (var key in attrs) {
            var value = attrs[key];
            if (key === "name") {
                if (value.length <= 0) {
                    return 'Error: You must enter a name!';
                }
            }
            if (key === "year") {
                if (value.length <= 0) {
                    return 'Error: You must enter a year!';
                }
            }
        }
      },
      
      // Define the defaults for a new record
      defaults: {
        name: "",
        grapes: "",
        country: "USA",
        region: "California",
        year: "2012",
        description: "",
        picture: null
      },
      
      // Debug helper function
      logInfo: function(prefix) {
        prefix = (undefined == prefix) ? "" : prefix + ":";
        return Ti.API.info(prefix + "wine id: " + this.get("id") 
          + " name: " + this.get("name") 
          + " description: " + this.get("description")
          );
      },  
      
      // Id generator function used when the adapter is sql and loaded from memorystore (not remote server/REST)
      newGUID: function() {
        function S4() {
         return ((1 + Math.random()) * 65536 | 0).toString(16).substring(1);
        };
        return S4() + S4() + "-" + S4() + "-" + S4() + "-" + S4() + "-" + S4() + S4() + S4();
      },
      
      // Helper function for saving a new model and adding to the collection
      saveAndAdd: function(collection, options) {
          // Save to persistent storage. 
          // If unsuccessful (eg. validation failed), we don't want it in the collection.
          this.save({
            error: function(){ 
              return Ti.API.error("Save failed");
            }
          });
          
          // Fallback if an ID wasn't set - e.g. loaded from memorystore
          if (this.get("id") == "") {
            this.set("id", this.newGUID());
            this.save({
              error: function(){ 
                return Ti.API.error("Save failed to set a GUID id field");
              }
            });
          } 
          
          // If save was successful and the collection is defined, add it.
          if (undefined !== collection) {        
            collection.add(this, options);  
          }      
      },
      
		}); // end extend
		
		return Model;
	},
	
	
	extendCollection: function(Collection) {		
		_.extend(Collection.prototype, {
		  
      url: Alloy.globals.urlRoot,
		  
		  // Sort function 
		  comparator: function(model1, model2) {
		    var name1 = model1.get("name").toUpperCase();
		    var name2 = model2.get("name").toUpperCase();
		    app.logInfo("Sort comparing: " + name1 + " to " + name2);
		    if (name1 == name2) {
		      return 0;
		    } else if (name1 < name2) {
		      return -1;
		    } else if (name1 > name2) {
		      return 1;
		    }
		  },
			
      // Debug helper function, show the collection count
      logInfo: function(prefix) {
        prefix = (undefined == prefix) ? "" : prefix + ":";
        return Ti.API.info(prefix + "wine recordcount: " + this.length);
      },    
      
      clearData: function() {
        // this.reset(); -- should empty the collection but doesn't?
        while (this.length > 0) {
          var wine = this.at(this.length-1);
          wine.destroy();
        } 
      },  

      // Populate data from memorystore.js if this is the first load
      populateData: function(reset) {
        if (true == reset) {
          this.clearData();
        }     
        var memorystore = require("memorystore");
        var wineData = memorystore.getWineList();
        var i = 1, len = wineData.length;
        for (; i < len; i++) {
          var wine = wineData[i];
          var newWine = Alloy.createModel("Wine", {
            name : wine.name,
            year : wine.year,
            grapes : wine.grapes,
            country : wine.country,
            region : wine.region,
            description : wine.description,
            picture : wine.picture,
          }); 
          newWine.saveAndAdd(this, {silent: true});          
        }  
      },      			
		}); // end extend
		
		return Collection;
	}		
}

