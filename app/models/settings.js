/*
 * The Backbone model for configuration and preference items
 * 
 * Note that the properties adapter behaves unusually 
 * - you need to specify the id otherwise it creates a new entry with each save (refer "defaults.id")
 */
exports.definition = {
  
  // All properties belonging to a configuration, as you'd find in an ini file
  // -- note that the properties adapter only has these simple types (String, Int etc)
  config: {
    "columns": {
      "id": "String",
      "adapterType": "String",
      "urlRoot": "String",
      "userName": "String",
      "hasEverAuthenticated": "Int", 
    },

    "defaults": {
      "id": "singleinstance",
      "adapterType": "restadapter",
      "urlRoot": "http://default/cellar/api/wines",
      "userName": "",
      "hasEverAuthenticated": 0,
    },
    
    "adapter": {
      "type": "properties",
      "collection_name": "settings",
    }
  }, 	
}