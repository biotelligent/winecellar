// Setup the common configuration and utilities for this project
//
var Alloy = require('alloy');
var _ = require("alloy/underscore")._;
var NavigationController = require('navigationcontroller').NavigationController;

// Singleton properties model 
exports.Settings = Alloy.Models.instance("settings"); 

// The singleton instance of the Wine collection, referenced as app.Wines
// Exported, but not initialised until settings are loaded so that we can switch the adapter at runtime
exports.Wines = null;

exports.initAdapter = function() {
  // (Re)initialise the adapter for the model after settings are loaded
  Alloy.globals.urlRoot = app.Settings.get("urlRoot");  // e.g. "http://jmac/cellar/api/wines";
  Alloy.globals.sqlAdapter = app.Settings.get("adapterType"); 

  app.logInfo("initialising adapter to " + Alloy.globals.sqlAdapter + " [url]: " + Alloy.globals.urlRoot, ["settings"]);
  app.Wines = Alloy.Collections.instance("wine");   
}

exports.isNull = function(obj) {
  var result = ((undefined === obj) || (null == obj)) ? true : false;
  return result;
};

exports.logInfo = function(text, /*array*/filters) {
  // Debugging logger function. Any filter specifying [br] will breakpoint, as alloy modules can't breakpoint.
  
  if ((!ENV_PRODUCTION) && (this.config.isDebugMode)) {
    filters = filters || [];

    // Tags or filters to ignore (hardcoded)
    var ignorefilters, breakpointfilters;
    ignorefilters = ["ignore", "trace"];
    breakpointfilters = ["br"];

    // Don't log/break if any of these filters are in our ignore list
    if (_.intersection(filters, ignorefilters).length > 0) {
      return;
    }   
    
    // Log to console, prefix filters. 
    var prefix = "";
    if (filters.length > 0) {
      prefix = "[" + filters.join(",") + "] ";
    }
    Ti.API.info(prefix + text);
    
    // Trigger a breakpoint. I allow multiple definitions for group en/disable.
    if (_.intersection(filters, breakpointfilters).length > 0) {
      debugger;
      return;
    }       
  }
};

/*
 * CONFIG
 */
exports.config = {
  appDisplayName : "Wine Cellar Demo",
  isDebugMode : true,
  showResetBtn : true,
};

/*
* DEVICE
*/
// nb. Conditional code generation in Alloy can now use OS_IOS and other constants
var platformName = Ti.Platform.osname;
exports.device = {
  osName : platformName,
  isiPhone : (platformName == 'iphone') ? true : false,
  isiPad : (platformName == 'ipad') ? true : false,
  isAndroid : (platformName == 'android') ? true : false,
  isiOS : ((platformName == 'iphone') || (platformName == 'ipad')),
};

exports.device.isOnline = function() {
  return Ti.Network.online;
};

exports.device.sdkVersion = function() {
  // e.g. 3.0
  var sdkVersion = parseFloat(Ti.version);
  return sdkVersion;
};

/*
 * UI
 */
exports.UI = {
  MainWindow : null,
  alertVisible: false,
};
exports.UI.navController = new NavigationController();

exports.UI.properCase = function(text) {
  return text ? text[0].toUpperCase() + text.substr(1) : text;
};

exports.UI.sameText = function(string1, string2) {
  string1 = string1 || "";
  string2 = string2 || "";
  return ((string1 == string2) || (string1.toUpperCase() == string2.toUpperCase())) ? true : false;
};

exports.UI.showSettingsWindow = function(currentSetting) {
  var winesettings = Alloy.createController("winesettings", {
    model: currentSetting
  });
  
  // Only open the settings page with the navController if it has been launched from another page
  // --If it is the first / landing page (no settings yet), we don't want to make it the home page
  if (this.navController.length > 0) {
    this.navController.open(winesettings.getView());
  } else {
    winesettings.getView().open();
  }
};

exports.UI.showListWindow = function(ownerWin) {
  app.logInfo("Showing list window", ["list"]);
  if (app.Wines == null) {
    app.initAdapter();
  }
  var winelist = Alloy.createController("winelist", {
    window : ownerWin
  });
  this.navController.open(winelist.getView());
};

exports.UI.showDetailWindow = function(wine, callbackOnClose) {
  if (undefined !== wine) {
    wine.logInfo("showDetailWindow");
    var winedetail = Alloy.createController("winedetail", {
      wine : wine,
      edit : false,
      onclose : callbackOnClose,
    });
    this.navController.open(winedetail.getView());
  }
  return;
};

exports.UI.showAlertDialog = function(message) {
  // Don't show a second alert if one has just been shown
  if (this.alertVisible) return;
  
  setTimeout((function(e){
    app.UI.alertVisible = false;
  }),
  500);
  
  this.alertVisible = true;
  alert(message);
  return;
};

exports.UI.showConfirmDialog = function(options, callbackOnConfirm, callbackOnCancel) {
  // Show a popup confirmation menu; (pass title and button names in options)
  var _this = this;
  var opts = {
    cancel : 1,
    selectedIndex : 1,
    destructive : 0,
    options : ['Confirm', 'Cancel'],
    title: 'Do you want to continue?',
  };
  _.extend(opts, options);

  var dialog = Ti.UI.createOptionDialog(opts);
  dialog.addEventListener('click', function(e) {
    app.logInfo('Option dialog selected ' + JSON.stringify(e));
    if ((e.index == opts.cancel) || (true === e.cancel)) {
      if (callbackOnCancel) {
        callbackOnCancel.call(_this, e);
      }
    } else {
      if (callbackOnConfirm) {
        callbackOnConfirm.call(_this, e);
      }
    }
  });
  dialog.show();

  return;
};

exports.UI.imageFile = function(fileName) {
  // Return the local image resource (for shipped images)
  var f = Ti.Filesystem.getFile(Ti.Filesystem.resourcesDirectory, '/images/' + fileName);
  if (f.exists()) {
    return f;
  } else {
    return null;
  }
};

exports.UI.widthScreen = function() {
  // Used for static layout calculations in absolute mode
  // nb. in a label, you'd set properties as {width: Ti.UI.SIZE} or {width: 'auto'} ,
  return Ti.Platform.displayCaps.platformWidth;
};

exports.UI.heightScreen = function() {
  return Ti.Platform.displayCaps.platformHeight;
};
